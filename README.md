### What is this repository for? ###
This repository is for robot and camera calibration. The calibration is tested with KUKA lbr4 arm and kinect2/asus xtion pro camera. 
This repo has two parts: calibration data collection and calibration.     

### Data Collection ###
Robot camera calibration takes data samples, including checkerboard images and robot end-effector frames, as inputs. 20-30 calibration data samples should be enough for calibration.     
    
Launch data collection server: `roslaunch robot_camera_calibration data_collection.launch`. Specify if you want to use kinect2 or asus camera in the launch file.   
    
Run data collection client `rosrun robot_camera_calibration data_client.py _data_path:=your_path`. Specify your data collection path with `data_path`. 
Robot joint states and end-effector poses are saved into a h5 file under the specified data path. Camera rgb, depth and pointcloud are also saved in the specifed folder.
Press Enter key to collect data.

### Calibration ###
Calibration is from [Torstein A. Myhre's awesome calibration code](http://folk.ntnu.no/torstem/snippets/robcam_calibration.html).    

Run calibration: `python run_calibration.py`. Need to specify `data_path` to read the calibration samples in the main function. 
You can change the paths to save robot camera tf and camera distortion/intrinsic parameters inside `run_calibration.py`. 
It is important to specify the chessboard parameters in `chessboard.py` if you use a different chessboard. 

Broadcast the calibration result: `rosrun robot_camera_calibration broadcast_calib_tf.py _use_kinect2:=True`. Change `use_kinect2` to false for asus camera. 
Need to specify the `calib_tf_path` inside `broadcast_calib_tf.py` for the file to read the calibrated tf.     

### Camera ###
* Kinect2:    
[Our Kinect2 calibration data](https://bitbucket.org/robot-learning/kinect2_calibration_files) is computed using [iai_kinect2](https://github.com/code-iai/iai_kinect2). 
However, the calibrated distortion parameters don't work well (e.g. hd RGB rect images have distortions around image edges). This distortion issue is solved by setting distortion coefficients to zero. 
Other kinect2 calibration parameters are not changed except distortion.    
Kinect2 has two raw outputs: hd rgb image and sd depth image. The hd rgb and sd depth needs to be rectified first. 
Then the sd depth rect is upsampled to get hd xyzrgb pointcloud and the hd rgb rect is downsampled to get the sd xyzrgb pointcloud.   
I used the hd images to do calibration for hd frame for kinect2.    

* Asus xtion pro:     
The `depth_registrtion` needs to be turned on in order to use asus xtion pro with its built-in calibration. Run `rosrun rqt_reconfigure rqt_reconfigure` to turn `depth_registration` on.   
The ros message filter time synchronizer doesn't work for asus. I don't know the reason yet. I used subscribers to get the rgb, depth and pointcloud for asus in the end.     

### Calibration Results ####
* Test calibration
`roslaunch urlg_robots_description lbr4_rviz.launch` from [urlg_robots_description]() to launch the robot model and robot state publisher. Broadcast the calibration tf.
Check if the robot in pointcloud overlaps with the robot model in rviz.  Add `<remap from="/joint_states" to="/lbr4/joint_states" />` for robot_state_publisher node in `lbr4_rviz.launch`.

* Accuracy. 
The calibrated orientation is accurate. But the translation does have small offsets, which is not hard to be manually tuned. I am not sure about the translation error source yet. It might be the checkerboard.   

### TO DO ###
The robot computer and perception computer have different ros timestamps. So the synchronizer doesn't work to get the camera data and robot data in synchronization. 
Need to set up the NTP server to synchronize the time between these two computers.     

### Maintainer ###
Qingkai Lu 
Email: qklu (at) cs (dot) utah [dot] edu 
