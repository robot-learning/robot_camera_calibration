#ifndef CALIB_DATA_COLLECTION_SERVER_H
#define CALIB_DATA_COLLECTION_SERVER_H
#include <ros/ros.h>
#include <pcl/common/common.h>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/JointState.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>

#include "robot_camera_calibration/DataCollection.h"

class CalibDataCollection{
    typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloudXYZRGB;
    //typedef message_filters::sync_policies::ExactTime<sensor_msgs::PointCloud2,
    //                                                 sensor_msgs::PointCloud2,
    //                                                 sensor_msgs::Image,
    //                                                 sensor_msgs::Image,
    //                                                 sensor_msgs::Image,
    //                                                 sensor_msgs::Image,
    //                                                 sensor_msgs::JointState> DataSyncPolicy;

    typedef message_filters::sync_policies::ExactTime<sensor_msgs::PointCloud2,
                                                     sensor_msgs::PointCloud2,
                                                     sensor_msgs::Image,
                                                     sensor_msgs::Image,
                                                     sensor_msgs::Image,
                                                     sensor_msgs::Image> Kinect2SyncPolicy;

    typedef message_filters::sync_policies::ExactTime<sensor_msgs::PointCloud2,
                                                     sensor_msgs::Image,
                                                     sensor_msgs::Image> AsusSyncPolicy;

    public:
        CalibDataCollection(ros::NodeHandle nd);

        /**
         *Callback function to get kinect2 data (hd and sd pointcloud, rgb, depth) and robot joint states.
         */
        void getDataCallback(const sensor_msgs::PointCloud2ConstPtr& msg_cloud,
                const sensor_msgs::PointCloud2ConstPtr& msg_sd_cloud,
                const sensor_msgs::ImageConstPtr& rgb_msg,
                const sensor_msgs::ImageConstPtr& sd_rgb_msg,
                const sensor_msgs::ImageConstPtr& depth_msg,
                const sensor_msgs::ImageConstPtr& sd_depth_msg,
                const sensor_msgs::JointStateConstPtr& lbr4_joint_state_msg);

        /**
         *Callback function to get kinect2 data (hd and sd pointcloud, rgb, depth).
         */
        void getKinect2DataCallback(const sensor_msgs::PointCloud2ConstPtr& msg_cloud,
                const sensor_msgs::PointCloud2ConstPtr& msg_sd_cloud,
                const sensor_msgs::ImageConstPtr& rgb_msg,
                const sensor_msgs::ImageConstPtr& sd_rgb_msg,
                const sensor_msgs::ImageConstPtr& depth_msg,
                const sensor_msgs::ImageConstPtr& sd_depth_msg);

        /**
         *Callback function to get asus data (pointcloud, rgb, depth).
         */
        void getAsusDataCallback(const sensor_msgs::PointCloud2ConstPtr& msg_cloud,
                const sensor_msgs::ImageConstPtr& rgb_msg,
                const sensor_msgs::ImageConstPtr& depth_msg);

        /**
        *Callback function to get pointcloud.
        */
        void getPcdCallback(const sensor_msgs::PointCloud2ConstPtr& msg_cloud);

        /**
         *Callback function to get rgb.
         */
        void getRgbCallback(const sensor_msgs::ImageConstPtr& rgb_msg);

        /**
         *Callback function to get depth.
         */
        void getDepthCallback(const sensor_msgs::ImageConstPtr& depth_msg);

        /**
         *Callback function to get robot joint states.
         */
        void getJointstatesCallback(const sensor_msgs::JointStateConstPtr& lbr4_joint_state_msg);

        /**
         * Convenience function to convert a depth image into a suitable format for saving and writes it to
         * disk
         *
         * \param depth_image 
         *      Image to save
         * \param depth_image_save_path
         *      The location to save the image
         */
        inline void saveDepthImage(cv::Mat& depth_image, std::string depth_image_save_path);

        /**
         * Convert the depth image to a format suitable for saving
         */
        inline void convertDepthImageToSaveFormat(cv::Mat& depth_image_in, cv::Mat& depth_image_out);

        /**
         *Save data service function.
         */
        bool saveData(robot_camera_calibration::DataCollection::Request& req,
                    robot_camera_calibration::DataCollection::Response& res);

    private:
        ros::NodeHandle n;
        ros::NodeHandle n_private;
        bool use_kinect2;
        std::string pcd_topic; 
        std::string sd_pcd_topic; 
        std::string rgb_topic; 
        std::string sd_rgb_topic; 
        std::string depth_topic; 
        std::string sd_depth_topic; 
        message_filters::Subscriber<sensor_msgs::PointCloud2> point_cloud_sub;
        message_filters::Subscriber<sensor_msgs::PointCloud2> point_cloud_sd_sub;
        message_filters::Subscriber<sensor_msgs::Image> depth_img_sub;
        message_filters::Subscriber<sensor_msgs::Image> rgb_img_sub;
        message_filters::Subscriber<sensor_msgs::Image> sd_depth_img_sub;
        message_filters::Subscriber<sensor_msgs::Image> sd_rgb_img_sub;
        message_filters::Subscriber<sensor_msgs::JointState> lbr4_joint_state_sub;
        //message_filters::Synchronizer<DataSyncPolicy> sync;
        boost::shared_ptr<message_filters::Synchronizer<Kinect2SyncPolicy> > kinect2_sync;
        boost::shared_ptr<message_filters::Synchronizer<AsusSyncPolicy> > asus_sync;

        ros::ServiceServer save_data_srv;
        PointCloudXYZRGB cur_cloud;
        PointCloudXYZRGB cur_sd_cloud;
        sensor_msgs::ImageConstPtr cur_depth;
        sensor_msgs::ImageConstPtr cur_rgb;
        sensor_msgs::ImageConstPtr cur_sd_depth;
        sensor_msgs::ImageConstPtr cur_sd_rgb;
        sensor_msgs::JointStateConstPtr cur_lbr4_joint_state;

};

#endif

