#!/usr/bin/env python

import roslib; roslib.load_manifest('robot_camera_calibration')
import rospy
import roslib.packages as rp
import tf
from geometry_msgs.msg import Pose, Quaternion, PoseStamped, PointStamped
from robot_camera_calibration.srv import *
import os
from sensor_msgs.msg import JointState, Image
import numpy as np
import time
import cv2
import sys
sys.path.append(rp.get_pkg_dir('robot_camera_calibration') +
                '/src/data_collection/robot_fk')
import robot_arm_fk
import h5py

class CalibDataClient:
    def __init__(self, init=True):
        rospy.init_node('calib_data_client')
        self.data_path = rospy.get_param('~data_path', '/home/qingkai/calib_data/')
        robot_urdf_path = rp.get_pkg_dir('robot_camera_calibration') + \
                '/src/data_collection/robot_fk/lbr4.robot.urdf'
        self.robot_fk = robot_arm_fk.RobotArmFk(robot_urdf_path) 
        self.robot_ee_file_path = self.data_path + 'robot_ee.h5'
        if init:
            self.intialize_robot_ee_file()

    def intialize_robot_ee_file(self):
        '''
        Initialize the root end-effector h5 file.
        '''
        #robot_ee_file = h5py.File(self.robot_ee_file_path, 'w')
        robot_ee_file = h5py.File(self.robot_ee_file_path, 'a')
        samples_num_key = 'samples_num'
        if samples_num_key not in robot_ee_file:
            robot_ee_file.create_dataset(samples_num_key, data=0)
        self.cur_sample_id = robot_ee_file[samples_num_key][()]
        robot_ee_file.close()

    def write_robot_ee_sample(self, joint_state, robot_ee_trans):
        '''
        Write robot arm joint states and robot end effector tansformation into 
        h5 file.
        '''
        #a: Read/write if exists, create otherwise (default)
        #'r+': Read/write, file must exist
        robot_ee_file = h5py.File(self.robot_ee_file_path, 'r+')
        samples_num = robot_ee_file['samples_num'][()]
        js_key = 'lbr4_js_' + str(samples_num)
        if js_key not in robot_ee_file:
            robot_ee_file.create_dataset(js_key, data=joint_state)
        ee_key = 'lbr4_ee_' + str(samples_num)
        if ee_key not in robot_ee_file:
            robot_ee_file.create_dataset(ee_key, data=robot_ee_trans)
        robot_ee_file['samples_num'][()] += 1
        robot_ee_file.close()

    def convert_trans_to_pose(self, trans_matrix):
        '''
        Convert transformation matrix to ROS PoseStamped.

        Args:
            trans_matrix

        Returns:
            ROS pose.
        '''
        pose_stamp = PoseStamped()
        pose_stamp.header.frame_id = 'base_link'
        pose_stamp.pose.position.x, pose_stamp.pose.position.y, \
                pose_stamp.pose.position.z = tf.transformations.translation_from_matrix(trans_matrix)  
        pose_stamp.pose.orientation.x, pose_stamp.pose.orientation.y, \
                pose_stamp.pose.orientation.z, pose_stamp.pose.orientation.w = \
                                        tf.transformations.quaternion_from_matrix(trans_matrix)
        return pose_stamp

    def update_robot_ee_pose_client(self, robot_ee_trans):
        '''
        Client to update the robot arm end-effector pose tf.
        '''
        #Convert robot end-effector transformation matrix to PoseStamped.
        
        rospy.loginfo('Waiting for service update_robot_ee_pose.')
        rospy.wait_for_service('update_robot_ee_pose')
        rospy.loginfo('Calling service update_robot_ee_pose.')
        try:
            update_robot_ee_pose_proxy = rospy.ServiceProxy('update_robot_ee_pose', UpdateRobotEePose)
            update_robot_ee_pose_request = UpdateRobotEePoseRequest()
            update_robot_ee_pose_request.robot_ee_pose = self.convert_trans_to_pose(robot_ee_trans)
            update_robot_ee_pose_response = update_robot_ee_pose_proxy(update_robot_ee_pose_request) 
        except rospy.ServiceException, e:
            rospy.loginfo('Service update_robot_ee_pose call failed: %s'%e)
        rospy.loginfo('Service update_robot_ee_pose is executed.')

    def data_collection_client(self, data_id):
        rospy.loginfo('Waiting for service calib_data_collection.')
        rospy.wait_for_service('calib_data_collection')
        rospy.loginfo('Calling service calib_data_collection.')
        try:
            data_collection_proxy = rospy.ServiceProxy('calib_data_collection', DataCollection)
            data_collection_request = DataCollectionRequest()
            data_collection_request.cloud_save_path = self.data_path + 'cloud_' + str(data_id) + '.pcd' 
            data_collection_request.sd_cloud_save_path = self.data_path + 'sd_cloud_' + str(data_id) + '.pcd' 
            data_collection_request.rgb_save_path = self.data_path + 'rgb_' + str(data_id) + '.png' 
            data_collection_request.sd_rgb_save_path = self.data_path + 'sd_rgb_' + str(data_id) + '.png' 
            data_collection_request.depth_save_path = self.data_path + 'depth_' + str(data_id) + '.png' 
            data_collection_request.sd_depth_save_path = self.data_path + 'sd_depth_' + str(data_id) + '.png' 
            self.data_collection_response = data_collection_proxy(data_collection_request)  
        except rospy.ServiceException, e:
            rospy.loginfo('Service calib_data_collection call failed: %s'%e)
        lbr4_js = self.data_collection_response.lbr4_joint_state.position
        robot_ee_trans = self.robot_fk.fk(lbr4_js)
        self.write_robot_ee_sample(lbr4_js, robot_ee_trans)
        self.update_robot_ee_pose_client(robot_ee_trans)
        rospy.loginfo('Service calib_data_collection is executed %s.' %str(self.data_collection_response.success))

if __name__ == '__main__':
    data_client = CalibDataClient()
    i = data_client.cur_sample_id
    while True:
        key_input = raw_input('Press enter to collect data.\n')
        #key_input will be empty if enter is pressed since the only way to exit raw_input
        #is to press enter key.
        if key_input == '':
            data_client.data_collection_client(i)
            i += 1

