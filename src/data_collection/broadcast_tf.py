#!/usr/bin/env python
import rospy
import tf
import numpy as np
from geometry_msgs.msg import Pose, Quaternion, PoseStamped, PointStamped
from robot_camera_calibration.srv import *

class BroadcastTf:
    '''
    The class to create ROS services to update and broadcast the robot arm end-effector tf.
    '''
    def __init__(self):
        rospy.init_node('broadcast_robot_ee_tf')
        self.robot_ee_pose = None #PoseStamped()
        self.tf_br = tf.TransformBroadcaster()

    def broadcast_tf(self):
        '''
        Broadcast the robot end-effector pose tf.
        '''
        if self.robot_ee_pose is not None:
            self.tf_br.sendTransform((self.robot_ee_pose.pose.position.x, self.robot_ee_pose.pose.position.y, 
                    self.robot_ee_pose.pose.position.z),
                    (self.robot_ee_pose.pose.orientation.x, self.robot_ee_pose.pose.orientation.y, 
                    self.robot_ee_pose.pose.orientation.z, self.robot_ee_pose.pose.orientation.w),
                    rospy.Time.now(), 'robot_ee_pose', self.robot_ee_pose.header.frame_id)

    def handle_update_robot_ee_pose(self, req):
        '''
        Handler to update the robot end-effector pose tf.
        '''
        self.robot_ee_pose = req.robot_ee_pose
        response = UpdateRobotEePoseResponse()
        response.success = True
        return response

    def update_robot_ee_pose_server(self):
        '''
        Create the ROS server to update the robot end-effector pose tf.
        '''
        rospy.Service('update_robot_ee_pose', UpdateRobotEePose, self.handle_update_robot_ee_pose) 
        rospy.loginfo('Service update_robot_ee_pose:')
        rospy.loginfo('Ready to update robot end-effector pose:')

if __name__ == '__main__':
    broadcast_tf = BroadcastTf() 
    broadcast_tf.update_robot_ee_pose_server()
    while not rospy.is_shutdown():
        broadcast_tf.broadcast_tf()
        rospy.sleep(1)


