#!/usr/bin/env python

import rospy
import roslib
from urdf_parser_py.urdf import Robot
from pykdl_utils.kdl_parser import kdl_tree_from_urdf_model
from pykdl_utils.kdl_kinematics import KDLKinematics

import numpy as np

#code based on : https://bitbucket.org/robot-learning/ll4ma_kdl

#_EE_NAME ='lbr4_7_link'
#_LINK_NAMES=['lbr4_0_link','lbr4_1_link','lbr4_2_link','lbr4_3_link','lbr4_4_link','lbr4_5_link','lbr4_6_link','lbr4_7_link']

class RobotArmFk:
    def __init__(self, urdf_file_name, base_link="lbr4_0_link", ee_name='lbr4_7_link'):
        '''
        Simple model of manipulator kinematics and controls
        Assume following state and action vectors
        urdf_file_name - model file to load
        '''
        # Load KDL tree
        urdf_file = file(urdf_file_name, 'r')
        self.robot = Robot.from_xml_string(urdf_file.read())
        urdf_file.close()
        self.tree = kdl_tree_from_urdf_model(self.robot)
        self.base_link = base_link
        self.chain = KDLKinematics(self.robot, self.base_link, ee_name)
    
    def fk(self, joint_angles):
        '''
        Method to convert joint positions to task coordinates
        '''
        return self.chain.forward(joint_angles)

