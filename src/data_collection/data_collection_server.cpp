#include "data_collection_server.h"
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/io/pcd_io.h>

#include <opencv2/highgui/highgui.hpp>
#include "data_collection_server.h"

CalibDataCollection::CalibDataCollection(ros::NodeHandle nd):n(nd), n_private("~"),
                                        point_cloud_sub(n, "/kinect2/hd/points", 1),
                                        point_cloud_sd_sub(n, "/kinect2/sd/points", 1),
                                        rgb_img_sub(n, "/kinect2/hd/image_color_rect", 1),
                                        sd_rgb_img_sub(n, "/kinect2/sd/image_color_rect" , 1),
                                        depth_img_sub(n, "/kinect2/hd/image_depth_rect", 1),
                                        sd_depth_img_sub(n, "/kinect2/sd/image_depth_rect", 1),
                                        lbr4_joint_state_sub(n, "/lbr4/joint_states", 1)
{
    n_private.param("use_kinect2", use_kinect2, true);
    if (use_kinect2)
    {
        kinect2_sync = boost::shared_ptr<message_filters::Synchronizer<Kinect2SyncPolicy> >(
                new message_filters::Synchronizer<Kinect2SyncPolicy>(Kinect2SyncPolicy(15), point_cloud_sub,
                                                            point_cloud_sd_sub, rgb_img_sub, 
                                                            sd_rgb_img_sub, depth_img_sub, sd_depth_img_sub));
        kinect2_sync->registerCallback(&CalibDataCollection::getKinect2DataCallback, this);
    }
    else
    {
        //Notice: synchronizer doesn't work for ASUS!!! Subscribers do work. Don't know why yet.
        //asus_sync = boost::shared_ptr<message_filters::Synchronizer<AsusSyncPolicy> >(
        //        new message_filters::Synchronizer<AsusSyncPolicy>(AsusSyncPolicy(15), point_cloud_sub,
        //                                    rgb_img_sub, depth_img_sub));
        //asus_sync->registerCallback(&CalibDataCollection::getAsusDataCallback, this);
        point_cloud_sub.registerCallback(&CalibDataCollection::getPcdCallback, this);
        rgb_img_sub.registerCallback(&CalibDataCollection::getRgbCallback, this);
        depth_img_sub.registerCallback(&CalibDataCollection::getDepthCallback, this);
    }

    lbr4_joint_state_sub.registerCallback(&CalibDataCollection::getJointstatesCallback, this);

    save_data_srv = n.advertiseService("calib_data_collection",
                                     &CalibDataCollection::saveData, this);
    ROS_INFO("Service calib_data_collection:");
    ROS_INFO("Ready to collect calibration data.");

}

void CalibDataCollection::getDataCallback(const sensor_msgs::PointCloud2ConstPtr& msg_cloud,
        const sensor_msgs::PointCloud2ConstPtr& msg_sd_cloud,
        const sensor_msgs::ImageConstPtr& rgb_msg,
        const sensor_msgs::ImageConstPtr& sd_rgb_msg,
        const sensor_msgs::ImageConstPtr& depth_msg,
        const sensor_msgs::ImageConstPtr& sd_depth_msg,
        const sensor_msgs::JointStateConstPtr& lbr4_joint_state_msg)
{
    std::cout << "Get data..." << std::endl;
    pcl::fromROSMsg(*msg_cloud, cur_cloud);
    std::cout << msg_cloud->header << std::endl;
    pcl::fromROSMsg(*msg_sd_cloud, cur_sd_cloud);
    cur_rgb = rgb_msg;
    cur_sd_rgb = sd_rgb_msg;
    cur_depth = depth_msg;
    cur_sd_depth = sd_depth_msg;
    cur_lbr4_joint_state = lbr4_joint_state_msg;
}

void CalibDataCollection::getKinect2DataCallback(const sensor_msgs::PointCloud2ConstPtr& msg_cloud,
        const sensor_msgs::PointCloud2ConstPtr& msg_sd_cloud,
        const sensor_msgs::ImageConstPtr& rgb_msg,
        const sensor_msgs::ImageConstPtr& sd_rgb_msg,
        const sensor_msgs::ImageConstPtr& depth_msg,
        const sensor_msgs::ImageConstPtr& sd_depth_msg)
{
    std::cout << "Get data..." << std::endl;
    pcl::fromROSMsg(*msg_cloud, cur_cloud);
    std::cout << "cloud header: " << msg_cloud->header << std::endl;
    pcl::fromROSMsg(*msg_sd_cloud, cur_sd_cloud);
    cur_rgb = rgb_msg;
    cur_sd_rgb = sd_rgb_msg;
    cur_depth = depth_msg;
    cur_sd_depth = sd_depth_msg;
}

void CalibDataCollection::getAsusDataCallback(const sensor_msgs::PointCloud2ConstPtr& msg_cloud,
        const sensor_msgs::ImageConstPtr& rgb_msg,
        const sensor_msgs::ImageConstPtr& depth_msg)
{
    std::cout << "Get asus data..." << std::endl;
    pcl::fromROSMsg(*msg_cloud, cur_cloud);
    std::cout << "cloud header: " << msg_cloud->header << std::endl;
    cur_rgb = rgb_msg;
    cur_depth = depth_msg;
}

void CalibDataCollection::getPcdCallback(const sensor_msgs::PointCloud2ConstPtr& msg_cloud)
{
    std::cout << "Get asus pcd..." << std::endl;
    pcl::fromROSMsg(*msg_cloud, cur_cloud);
    std::cout << "cloud header: " << msg_cloud->header << std::endl;
}

void CalibDataCollection::getRgbCallback(const sensor_msgs::ImageConstPtr& rgb_msg)
{
    std::cout << "Get asus rgb..." << std::endl;
    cur_rgb = rgb_msg;
}

void CalibDataCollection::getDepthCallback(const sensor_msgs::ImageConstPtr& depth_msg)
{
    std::cout << "Get asus depth..." << std::endl;
    cur_depth = depth_msg;
}



void CalibDataCollection::getJointstatesCallback(const sensor_msgs::JointStateConstPtr& lbr4_joint_state_msg)
{
    std::cout << "joint_state header: " << lbr4_joint_state_msg->header << std::endl;
    cur_lbr4_joint_state = lbr4_joint_state_msg;
}

inline void CalibDataCollection::convertDepthImageToSaveFormat(cv::Mat& depth_image_in, cv::Mat& depth_image_out)
{
    depth_image_out.create(depth_image_in.size(), CV_16UC1);
    depth_image_in.convertTo(depth_image_out, CV_16UC1);
}


inline void CalibDataCollection::saveDepthImage(cv::Mat& depth_image, std::string depth_image_save_path)
{
  cv::Mat depth_save_image;
  convertDepthImageToSaveFormat(depth_image, depth_save_image);
  imwrite(depth_image_save_path, depth_save_image);
}

bool CalibDataCollection::saveData(robot_camera_calibration::DataCollection::Request& req,
            robot_camera_calibration::DataCollection::Response& res)
{
    pcl::io::savePCDFile(req.cloud_save_path, cur_cloud, true);

    cv_bridge::CvImagePtr cv_rgb_msg = cv_bridge::toCvCopy(cur_rgb, sensor_msgs::image_encodings::BGR8);
    imwrite(req.rgb_save_path, cv_rgb_msg->image);

    cv::Mat depth_image;
    cv_bridge::CvImagePtr cv_depth_msg = cv_bridge::toCvCopy(cur_depth);
    cv_depth_msg->image.convertTo(depth_image, CV_32FC1);
    saveDepthImage(depth_image, req.depth_save_path);

    if(use_kinect2)
    {
        pcl::io::savePCDFile(req.sd_cloud_save_path, cur_sd_cloud, true);

        cv_bridge::CvImagePtr cv_sd_rgb_msg = cv_bridge::toCvCopy(cur_sd_rgb, sensor_msgs::image_encodings::BGR8);
        imwrite(req.sd_rgb_save_path, cv_sd_rgb_msg->image);

        cv::Mat sd_depth_image;
        cv_bridge::CvImagePtr cv_sd_depth_msg = cv_bridge::toCvCopy(cur_sd_depth);
        cv_sd_depth_msg->image.convertTo(sd_depth_image, CV_32FC1);
        saveDepthImage(sd_depth_image, req.sd_depth_save_path);
    }

    res.lbr4_joint_state.header = cur_lbr4_joint_state->header;
    res.lbr4_joint_state.name = cur_lbr4_joint_state->name;
    res.lbr4_joint_state.position = cur_lbr4_joint_state->position;
    res.lbr4_joint_state.velocity = cur_lbr4_joint_state->velocity;
    res.lbr4_joint_state.effort = cur_lbr4_joint_state->effort;

    res.success = true;
    
    return true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "calib_data_node");
    ros::NodeHandle n;
    CalibDataCollection data_collection(n);

    ros::spin();

    return 0;
}
