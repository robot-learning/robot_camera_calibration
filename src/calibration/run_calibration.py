import cPickle as pickle
import numpy
import chessboard
import park_martin
import yaml
numpy.set_printoptions(linewidth=300)
from scipy.linalg import expm3, inv
from numpy import dot, eye
import cv2
import h5py
import sys
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir('robot_camera_calibration') +
                '/src/data_collection')
import data_client as dc
import tf

def tf_mat(r, t):
    res = eye(4)
    res[0:3, 0:3] = expm3([[   0, -r[2],  r[1]],
                           [r[2],     0, -r[0]],
                           [-r[1],  r[0],    0]])
    res[0:3, -1] = t
    return res

def read_img_rob_poses(data_path):
    img_list = []
    rob_pose_list = []
    robot_ee_file_path = data_path + 'robot_ee.h5'
    robot_ee_file = h5py.File(robot_ee_file_path, 'r')
    samples_num = robot_ee_file['samples_num'][()]
    #samples_num = 10
    for i in xrange(samples_num):
        #if i in set([11, 15]):
        #    continue
        img_path = data_path + 'rgb_' + str(i) + '.png'
        #img = cv2.imread(img_path, cv2.IMREAD_COLOR)
        img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
        img_list.append(img)
        ee_key = 'lbr4_ee_' + str(i)
        robot_ee = robot_ee_file[ee_key][()]
        rob_pose_list.append(robot_ee)
    robot_ee_file.close()

    return img_list, rob_pose_list

def run_calibration(data_path):
    #img_list = pickle.load(open('image_list.dump'))
    #rob_pose_list = pickle.load(open('pose_list.dump'))
    img_list, rob_pose_list = read_img_rob_poses(data_path)
    #print img_list
    #print rob_pose_list
    corner_list = []
    obj_pose_list = []
    
    camera_matrix, dist_coeffs = chessboard.calibrate_lens(img_list)

    for i, img in enumerate(img_list):
        #cv2.imshow('chessboard', img)
        #cv2.waitKey(0)
        #cv2.imwrite('/home/qingkai/images/chessboard_' + str(i) + '.png', img)
        found, corners = chessboard.find_corners(img)
        corner_list.append(corners)
        if not found:
            raise Exception("Failed to find corners in img # %d" % i)
        rvec, tvec = chessboard.get_object_pose(chessboard.pattern_points, corners, camera_matrix, dist_coeffs)
        object_pose = tf_mat(rvec, tvec)
        obj_pose_list.append(object_pose)
    
    A, B = [], []
    for i in range(1,len(img_list)):
        p = rob_pose_list[i-1], obj_pose_list[i-1]
        n = rob_pose_list[i], obj_pose_list[i]
        A.append(dot(inv(p[0]), n[0]))
        B.append(dot(inv(p[1]), n[1]))
    
    # Transformation to chessboard in robot gripper
    cie = eye(4)
    Rx, tx = park_martin.calibrate(A, B)
    cie[0:3, 0:3] = Rx
    cie[0:3, -1] = tx
    
    # Seems averaging the euler angles is not the right way to 
    # compute the averaged orientation.
    #calib_client = dc.CalibDataClient(init=False)
    #avg_trans = numpy.zeros(3)
    #avg_euler = numpy.zeros(3)
    ## Compute transformations to camera.
    ## All the transformations should be quite similar
    #for i in range(len(img_list)):
    #    rob = rob_pose_list[i]
    #    obj = obj_pose_list[i]
    #    trans_matrix = dot(rob, dot(cie, inv(obj)))
    #    print(trans_matrix)
    #    translation = tf.transformations.translation_from_matrix(trans_matrix) 
    #    avg_trans += translation
    #    euler = tf.transformations.euler_from_matrix(trans_matrix)
    #    avg_euler += euler
    #    pose = calib_client.convert_trans_to_pose(trans_matrix)
    #    print pose
    #avg_trans /= len(img_list)
    #avg_euler /= len(img_list)
    #avg_quat = tf.transformations.quaternion_from_euler(avg_euler[0], avg_euler[1], avg_euler[2])
    #avg_tf = numpy.concatenate((avg_trans, avg_quat))
    ##Write robot camera tf.
    #numpy.save('./results/calib_tf', avg_tf)
    #print 'avg_trans:', avg_trans
    #print 'avg_euler:', avg_euler
    #print 'avg_quat', avg_quat
    
    calib_client = dc.CalibDataClient(init=False)
    sample_idx = int(len(img_list) / 2)
    rob = rob_pose_list[sample_idx]
    obj = obj_pose_list[sample_idx]
    trans_matrix = dot(rob, dot(cie, inv(obj)))
    print(trans_matrix)
    translation = tf.transformations.translation_from_matrix(trans_matrix) 
    quaternion = tf.transformations.quaternion_from_matrix(trans_matrix)
    print translation
    print quaternion
    numpy.save('./results/calib_tf', numpy.concatenate((translation, quaternion)))
    pose = calib_client.convert_trans_to_pose(trans_matrix)
    print pose

    #Write camera distortion coefficients and intrinsic parameters.
    cam_intrinsic = {'camera_matrix' : camera_matrix.tolist(),
           'dist_coeffs' : dist_coeffs.tolist()}
    fp = open('./results/camera_intrinsic.yaml', 'w')
    fp.write(yaml.dump(cam_intrinsic))
    fp.close()

if __name__ == '__main__':
    #Calibration data path.
    data_path = '/home/qingkai/calib_data/'
    run_calibration(data_path)

