#!/usr/bin/env python

import rospy
import sys
import tf
import tf2_ros
import geometry_msgs.msg
import numpy as np

import sys
import roslib.packages as rp

if __name__ == '__main__':
    rospy.init_node('calib_tf_broadcaster')
    use_kinect2 = rospy.get_param('~use_kinect2', True)
    broadcaster = tf2_ros.StaticTransformBroadcaster()
    static_transformStamped = geometry_msgs.msg.TransformStamped()

    static_transformStamped.header.stamp = rospy.Time.now()
    static_transformStamped.header.frame_id = "world"
    if use_kinect2:
        #Broadcast kinect2 hd tf.
        static_transformStamped.child_frame_id = 'kinect2_rgb_optical_frame'
    else:
        #Broadcast asus camera tf.
        static_transformStamped.child_frame_id = 'camera_rgb_optical_frame'

    #Load calibration result. 
    calib_tf_path = rp.get_pkg_dir('robot_camera_calibration') + \
            '/src/calibration/results/calib_tf.npy'
    calib_tf = np.load(calib_tf_path)

    #The calibration adjustment making the arm align well doesn't aligh the hand well.
    #If the hand is aligned well, then the arm is not aligned well.

    #Arm calibration results with manual adjustment for kinect2 hd frame.
    static_transformStamped.transform.translation.x = calib_tf[0] - 0.025
    static_transformStamped.transform.translation.y = calib_tf[1] - 0.02
    static_transformStamped.transform.translation.z = calib_tf[2] - 0.05

    #Hand adjustment. 
    #static_transformStamped.transform.translation.x = calib_tf[0] + 0.1
    #static_transformStamped.transform.translation.y = calib_tf[1] + 0.02
    #static_transformStamped.transform.translation.z = calib_tf[2] + 0.015

    static_transformStamped.transform.rotation.x = calib_tf[3]
    static_transformStamped.transform.rotation.y = calib_tf[4]
    static_transformStamped.transform.rotation.z = calib_tf[5]
    static_transformStamped.transform.rotation.w = calib_tf[6]

    broadcaster.sendTransform(static_transformStamped)
    rospy.spin()
