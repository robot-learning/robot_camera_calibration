#!/usr/bin/env python
"""
Calibrates one camera to another camera by finding a shared marker.
"""
import rospy
from geometry_msgs.msg import PoseStamped
import tf
import time
import numpy
class CalibrateCam2Cam:
    def __init__(self, camera_frame_1, camera_frame_2):
        """
        Class Constructor
        """
        self.cf1 = camera_frame_1
        self.cf2 = camera_frame_2

        self.tf = tf.TransformListener()
        
        pose1_topic = "cam1_pose"
        pose2_topic = "cam2_pose"
        
        #rospy.wait_for_message(pose1_topic,PoseStamped)
        #rospy.wait_for_message(pose2_topic,PoseStamped)
        self.pose1_sub = rospy.Subscriber(pose1_topic, PoseStamped, self.pose1_cb)
        self.pose2_sub = rospy.Subscriber(pose2_topic, PoseStamped, self.pose2_cb)
        self.got_pose1 = False
        self.got_pose2 = False

    def pose1_cb(self, pose_data):
        if self.got_pose1:
            pass
        self.pose1 = pose_data
        if self.tf.frameExists(self.cf1):
            self.pose1 = self.tf.transformPose(self.cf1,self.pose1)
            self.got_pose1 = True
            print "Got pose for camera 1"
        else:
            print "Camera 1 Frame Doesn't Exsist"

    def pose2_cb(self, pose_data):
        if self.got_pose2:
            return
        self.pose2 = pose_data
        self.pose2.header.frame_id = self.pose2.header.frame_id[1:]
        print self.pose2.header.frame_id
        if self.tf.frameExists(self.cf2):
            self.pose2 = self.tf.transformPose(self.cf2,self.pose2)
            self.got_pose2 = True
            print "Got pose for camera 2"
        else:
            print "Camera frame 2 doesn't exsist"

    def calibrate(self):
        """
        Calibrates one camera to the other camera
        """
        print "Waiting for pose data"
        while not (self.got_pose1 and self.got_pose2):
            #print self.got_pose1
            #print self.got_pose2
            #print self.got_pose1 and self.got_pose2
            pass
        print "Data Recieved"
	# make sure both are in given frames
        # final rotation is q1 * q2^-1
        p1o = self.pose1.pose.orientation
        q1 = [p1o.x, p1o.y, p1o.z, p1o.w]
        p2o = self.pose2.pose.orientation
        q2 = [ p2o.x, p2o.y, p2o.z, p2o.w]
        q2_inv = tf.transformations.quaternion_inverse(q2)
        q_final = tf.transformations.quaternion_multiply(q1,q2_inv)
        
        # get final pose transform
        p1t = self.pose1.pose.position
        t1 = numpy.matrix([[p1t.x],[p1t.y],[p1t.z],[1.0]])
        p2t = self.pose2.pose.position
        t2 = numpy.matrix([[p2t.x],[p2t.y],[p2t.z],[1.0]])
        R_12 = tf.transformations.quaternion_matrix(q_final)
        t2_1 = R_12 * t2
        trans_f = t1 - t2_1
        print "From ", self.pose1.header.frame_id, " to ", self.pose2.header.frame_id
        print "Final Quaternion:", q_final
        print "Final Transform:",  trans_f.transpose()
        print '"', trans_f[0], " ", trans_f[1], " ", trans_f[2], " ", q_final[0], " ", q_final[1], " ", q_final[2], " ", q_final[3]

if __name__ == "__main__":
    rospy.init_node("Cam2Cam_Calibrator")
    camera_frame_1 = rospy.get_param("~camera_frame_1")
    camera_frame_2 = rospy.get_param("~camera_frame_2")

    c2c = CalibrateCam2Cam(camera_frame_1, camera_frame_2)
    c2c.calibrate()

    #rospy.spin()
                        
