#!/usr/bin/env python
"""
Locates a chessboard from an image and publishes its pose in that frame
"""
import roslib
#roslib.load_manifest('my_package')
import sys
import rospy
import cv2
import tf
from std_msgs.msg import String
from sensor_msgs.msg import Image
from sensor_msgs.msg import CameraInfo
from geometry_msgs.msg import PoseStamped
from cv_bridge import CvBridge, CvBridgeError

import time
import numpy

"""
To-Do
- Read in camera info from topic
"""


class ChessboardPoseEstimator:
    def __init__(self, rows, cols, size, display_chessboard, camera_info_topic):
        """
        Creates a subscriber to image topic
        Creates a publisher to a stamped pose topic
        """
        camera_topic      = "camera_image"
        #camera_info_topic = "camera_info"
        pose_topic        = "chessboard_pose"

        
        self.display_chessboard = display_chessboard
        self.camera_matrix = numpy.float32([[570.3422241210938, 0.0, 319.5],
                                         [0.0, 570.3422241210938, 239.5], 
                                         [0.0, 0.0, 1.0]])
        #print self.camera_matrix
        self.dist_coeff    = numpy.float32([0.0, 0.0, 0.0, 0.0, 0.0])

        #print "Waiting for message"
        msg = rospy.wait_for_message(camera_info_topic,CameraInfo)
        #print msg
        self.dist_coeff = msg.D
        #print self.dist_coeff
        self.camera_matrix = numpy.float32(numpy.resize(msg.K,(3,3)))
        #print self.camera_matrix
        print camera_info_topic
        if camera_info_topic == "/cameras/left_hand_camera/camera_info":
            self.dist_coeff = numpy.float32([0.001039, -0.037605, 0.002625, -0.006117, 0.000000])
            self.camera_matrix = numpy.float32([[399.703181, 0.000000, 324.547363],
                                                [0.000000, 398.146797, 228.130284],
                                                [0.000000, 0.000000, 1.000000]])
            #392.829468 0.000000 319.166614 0.000000
            #0.000000 396.395447 228.758504 0.000000
            #0.000000 0.000000 1.000000 0.000000
        print self.dist_coeff
        print self.camera_matrix
        # chessboard stuff
        self.pattern_size = (rows,cols)
        self.square_size = size
        
        self.pattern_points = numpy.zeros( (numpy.prod(self.pattern_size), 3), numpy.float32 )
        self.pattern_points[:,:2] = numpy.indices(self.pattern_size).T.reshape(-1, 2)
        self.pattern_points *= self.square_size

        self.pose_pub = rospy.Publisher(pose_topic, PoseStamped, queue_size =1)
        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber(camera_topic, Image, self.imageCB)

    def imageCB(self,data):
        try:
            image = self.bridge.imgmsg_to_cv2(data,"bgr8")
        except CvBridgeError as e:
            print(e)

        grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        found, corners = self.find_corners(grey)
        if(found):
            #print "Found Points"
            color_image = self.draw_corners(grey, corners)
            rot, tran = self.get_object_pose(self.pattern_points, corners, 
                                             self.camera_matrix, self.dist_coeff)
            rot2,holder = cv2.Rodrigues(rot)
            #print "Rotation:"
            #print rot2
            rotMat = numpy.append(rot2,[[0],[0],[0]],1)
            rotMat = numpy.append(rotMat,[[0,0,0,1.0]],0)
            #print "Rotation Mat Full:"
            #print rotMat
            q = tf.transformations.quaternion_from_matrix(rotMat)
            #print "\tQuaternion:", q
            #print "\tTranslation:", tran
            chessboard_pose = PoseStamped()
            chessboard_pose.header = data.header
            chessboard_pose.pose.position.x = tran[0]
            chessboard_pose.pose.position.y = tran[1]
            chessboard_pose.pose.position.z = tran[2]
        
            chessboard_pose.pose.orientation.x = q[0]
            chessboard_pose.pose.orientation.y = q[1]
            chessboard_pose.pose.orientation.z = q[2]
            chessboard_pose.pose.orientation.w = q[3]
            self.pose_pub.publish(chessboard_pose)

        #cv2.imshow("Raw Image", image)
        if(self.display_chessboard and found):
            cv2.imshow("Corner Image",color_image)
            cv2.waitKey(3)
        

    def find_corners(self,image):
        found, corners = cv2.findChessboardCorners(image, self.pattern_size)
        term = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1)
        if found:
            cv2.cornerSubPix(image, corners, (5, 5), (-1, -1), term)
        return found, corners

    def draw_corners(self, image, corners):
        color_image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
        cv2.drawChessboardCorners(color_image, self.pattern_size, corners, True)
        return color_image

    def get_object_pose(self, object_points, image_points, camera_matrix, dist_coeffs):
        ret, rvec, tvec = cv2.solvePnP(object_points, image_points, camera_matrix, dist_coeffs)
        return rvec.flatten(), tvec.flatten()


if __name__ == "__main__":
    rospy.init_node('Chessboard_Pose_Estimator')
    rows               = rospy.get_param("~chessboard_rows")
    cols               = rospy.get_param("~chessboard_cols")
    size               = rospy.get_param("~chessboard_size")
    display_chessboard = rospy.get_param("~display_chessboard")
    camera_info_topic  = rospy.get_param("~camera_info_topic")
    pose_est = ChessboardPoseEstimator(rows, cols, size, display_chessboard, camera_info_topic)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print "Shutting Down"
    cv2.destroyAllWindows()
